package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model;

import java.io.Serializable;

/**
 * BitbucketPullRequest
 *
 * 
 * <br /><br />
 * Created on 11.12.2012, 14:02:57
 * <br /><br />
 * @author jhocman@atlassian.com
 *
 */
public class BitbucketPullRequestApprovalActivity extends BitbucketPullRequestBaseActivity implements Serializable
{

    private static final long serialVersionUID = -7471604882753174823L;

    public BitbucketPullRequestApprovalActivity()
    {
        super();
    }

}

